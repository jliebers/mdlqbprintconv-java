/**
 * 
 */
package mdlqbprintconv;

/**
 * @author joo
 *
 */
public abstract class OutputWriter {
	public abstract void openDocument(String outFile);
	public abstract void printDocumentHeader();
	public abstract void printQuestion(Question q);
	public abstract void printDocumentFooter();
	public abstract void closeDocument();
}
