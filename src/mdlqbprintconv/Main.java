package mdlqbprintconv;

/**
 * @author joo
 *
 */
public class Main {
	
	/**
	 * @param inFile, outFile
	 */
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
//		String inFile = "/home/joo/Desktop/mdlqbprintconv/quiz.xml";
//		String outFile = "/home/joo/Desktop/mdlqbprintconv/quiz.rtf";
		
		String inFile = args[0];
		String outFile = args[1];
		String mode = args[2];	
		
		OutputWriter writer = null;
		
		switch (mode) {
		case "rtf":
			writer = new RTFWriter(outFile);
			break;
		case "html":
			writer = new HTMLWriter(outFile);
			break;
		case "default":
			System.exit(-1);
			break;
		} 

		QuizContentHandler handler = new QuizContentHandler(writer);
		QuizParser reader = new QuizParser(inFile, handler);
		
		reader.parseQuiz();
		
		System.out.println("Moodle Quiz Converter finished converting into " + mode 
				+ " after " + (System.currentTimeMillis() - startTime) + " ms");
	}
	
	
}
