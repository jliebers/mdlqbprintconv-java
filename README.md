# mdlqbprintconv-java

This repository holds the code for the *m*oo*dl*e *q*uestion*b*ank *print* *conv*erter.
It is a small application written in Java with a GUI in Swing, that is able to open
moodle questionbank export files and convert them into HTML and RTF, for the purpose
of being printed.

The source code can be checked out and compilation is a straight-forward process, 
as no external dependencies exist. The main class is found at `src/mdlqbprintconv/GUI.java`.

Currently the translation is hard-coded in German.