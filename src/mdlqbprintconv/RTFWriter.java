package mdlqbprintconv;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class RTFWriter extends OutputWriter {
	
	private Writer writer;
	private Question currentQuestion;

	public RTFWriter(String outFile) { // ctor
		openDocument(outFile);
	}
	
	public void openDocument(String outFile){
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "ISO-8859-1")); //Cp1252?
			
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void printDocumentHeader(){
//		System.out.println("+++ BEGIN DOCUMENT +++");
		try {
			writer.write("{\\rtf1\\n"); // \ansi vor \n
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void printDocumentFooter(){
//		System.out.println("+++ END OF DOCUMENT +++");
		
		try {
			writer.write("}");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void printQuestion(Question q){
		// do not print twice:
		if (q == currentQuestion)
			return;
		currentQuestion = q;

		
		try {
			writer.write("{\\b [" + q.getTranslatedType() + "]" + " " + q.getName() + "}\n\\line\n");
			writer.write("Frage: " + q.getText().trim().replaceAll(" +", " ") + "\n\\line\n");
			writer.write("Feedback(s): " + q.getFeedback().trim().replaceAll("\n", "").replaceAll(" +", " ") + "\n");
			
			writer.write("\\line\n\\line\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
//		System.out.println("[" + q.getTranslatedType() + "]" + " " + q.getName());
//		System.out.println("Frage: " + q.getText());
//		System.out.println("Feedback(s): " + q.getFeedback());
		
//		System.out.println("\n---\n");
	}

	public void closeDocument() {
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


