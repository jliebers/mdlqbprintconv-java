package mdlqbprintconv;

import java.io.FileReader;
import java.io.IOException;

import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class QuizParser {
	
	private String inFile;
	private ContentHandler handler;
	
	public QuizParser(String inFile, ContentHandler handler) {
		this.inFile = inFile;
		this.handler = handler;
	}

	public void parseQuiz() {
		try {
			// open file path:
			FileReader reader = new FileReader(inFile);
		    InputSource inputSource = new InputSource(reader);

			// create parser and set contentHandler:	
			XMLReader parser = XMLReaderFactory.createXMLReader();
			parser.setContentHandler(handler);
			
			// parse:	
			parser.parse(inputSource);
		} 
		catch (SAXException e) {
			System.err.println(e.getMessage());
		}
		catch (IOException e) {
			System.err.println(e.getMessage());
		}
		
	}
	
}
