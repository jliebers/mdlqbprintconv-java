package mdlqbprintconv;

public class Question {
	private String type;
	private String name;
	private String text;
	private String feedback;
	
	public String getTranslatedType() {
		switch(type) {
		case "truefalse":
			return "Wahr-Falsch";
		case "essay":
			return "Freitext";
		case "numerical":
			return "Numerisch";
		case "shortanswer":
			return "Kurzantwort";
		default:
			break;
		}
		
		return type;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getFeedback() {
		return feedback == null ? " " : feedback;
	}
	
	public void setFeedback(String feedback) {
		// TODO Add seperators for when multiple feedbacks are concatenated.
		this.feedback = feedback.trim();
	}
	
	public boolean isComplete() {
		return !(type == null || name == null || text == null || feedback == null);
	}
}
