package mdlqbprintconv;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.UIManager;
import javax.swing.SwingConstants;

public class GUI implements ActionListener {

	private JFrame frame;
	private JTextField txtFieldOpen, txtFieldSave;
	private JButton btnOpen, btnSave, btnConvert;
	private JFileChooser fcOpen, fcSave;
	private FileNameExtensionFilter xmlfilter;
	private JLabel lblOk;
	private String mode = "html";
	private JMenuItem mnItemStartConvert;
	private String version = "0.5";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new GUI();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
		this.frame.setVisible(true);
		this.frame.setResizable(false);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Moodle Quiz Druckconverter " + version);
		frame.setBounds(300, 300, 512, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
//		ImageIcon img = new ImageIcon("icon.png"); FIXME iconimage
//		frame.setIconImage(img.getImage());
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("Datei");
		JMenu mnFormat = new JMenu("Ausgabeformat");
		JMenu mnAbout = new JMenu("Über");
		menuBar.add(mnFile);
		menuBar.add(mnFormat);
		menuBar.add(mnAbout);
		
		// File-Menu:
		mnItemStartConvert = new JMenuItem("Konvertierung beginnen"); // Listener is below

		JMenuItem mnItemCloseProgram = new JMenuItem("Beende");
		mnItemCloseProgram.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				System.exit(0);
			}
		});
		mnFile.add(mnItemStartConvert);
		mnFile.add(new JSeparator());
		mnFile.add(mnItemCloseProgram);
		
		// Format-Menu:
		final JRadioButtonMenuItem mntmSettingsHTML = new JRadioButtonMenuItem("Ausgabe: HTML (bessere Ausgabe/nicht bearbeitbar)");
		mntmSettingsHTML.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				mode = "html";
			}
		});
		
		// FIXME reenable rtf
		final JRadioButtonMenuItem mntmSettingsRTF = new JRadioButtonMenuItem("Ausgabe: RTF (in dieser Version noch nicht verfügbar)");
		//final JRadioButtonMenuItem mntmSettingsRTF = new JRadioButtonMenuItem("Ausgabe: RTF (bearbeitbar/experimentell)");
		mntmSettingsRTF.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				mode = "rtf";
			}
		});
		mnFormat.add(mntmSettingsHTML);
		mnFormat.add(mntmSettingsRTF);
		mntmSettingsRTF.setEnabled(false); // FIXME enable rtf again
		
		// About-Menu:
		JMenuItem mntmNewMenuItem = new JMenuItem("Hilfe / FAQ");
		mntmNewMenuItem.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				JOptionPane.showMessageDialog(null, ""
						+ "<html>"
						+ "<h1>FAQ</h1>"
						+ "<h2>Welche Fragetypen werden unterst&uuml;tzt?</h2>"
						+ "<p>Momentan kann dieses Programm folgende Fragetypen konvertieren: #TODO</p>"
						+ "<h2>ASDF</h2>"
						+ "</html>");
			}
		});
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Über");
		mntmNewMenuItem_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				JOptionPane.showMessageDialog(null, "<html>"
						+ "<p>Autor: Liebers, Jonathan</p>"
						+ "<p>Kontakt: jonathan.liebers@uni-due.de</p>"
						+ "<p>Version: "+version+"</p>"
						+ "</html>");
			}
		});
		//mnAbout.add(mntmNewMenuItem); FIXME about disabled
		mnAbout.add(mntmNewMenuItem_1);
		frame.getContentPane().setLayout(null);
		
		JLabel lblWelcome = new JLabel(
				  "<html><body><b>Willkommen!</b>"
				  + "<br><br>"
				  + "Dieses Programm dient zur Konvertierung von Moodle-Tests im XML-Dateiformat in druckbare Dokumentformate. "
				  + "Das Ausgabeformat kann in der Men&uuml;leiste eingestellt werden."
				  + "<br><br>"
				  + "Bitte laden Sie zun&auml;chst die XML-Datei herunter:<br>"
				  + "<i>Moodle-Kursraum &#8594; Fragensammlung &#8594; Export &#8594; Moodle-XML-Format</i>"
				  + "<br><br>"
				  + "Bitte geben Sie den Dateipfad der Moodle-XML-Datei an:"
				  + "</body></html>"
				);
		lblWelcome.setVerticalAlignment(SwingConstants.TOP);
		lblWelcome.setBounds(12, 12, 488, 169);
		lblWelcome.setFont(UIManager.getFont("List.font"));
		frame.getContentPane().add(lblWelcome);
		
		btnOpen = new JButton("Öffne XML ...");
		btnOpen.setBounds(12, 184, 130, 23);
		btnOpen.setFont(new Font("Dialog", Font.BOLD, 11));
		frame.getContentPane().add(btnOpen);
		
		txtFieldOpen = new JTextField();
		txtFieldOpen.setBounds(155, 184, 335, 24);
		txtFieldOpen.setEditable(false);
		frame.getContentPane().add(txtFieldOpen);
		txtFieldOpen.setColumns(10);
		
		btnSave = new JButton("Speichere ...");
		btnSave.setBounds(12, 267, 130, 23);
		btnSave.setFont(new Font("Dialog", Font.BOLD, 11));
		frame.getContentPane().add(btnSave);
		
		txtFieldSave = new JTextField();
		txtFieldSave.setBounds(155, 267, 335, 24);
		txtFieldSave.setEditable(false);
		frame.getContentPane().add(txtFieldSave);
		txtFieldSave.setColumns(10);
		
		btnConvert = new JButton("Starte Konvertierung!");
		btnConvert.setBounds(155, 309, 200, 25);
		frame.getContentPane().add(btnConvert);
		btnConvert.setEnabled(false);
		
		lblOk = new JLabel("Ok!");
		lblOk.setBounds(384, 303, 36, 36);
		lblOk.setForeground(new Color(0, 100, 0));
		lblOk.setVisible(false);
		frame.getContentPane().add(lblOk);
		
		JLabel lblBitteGebenSie = new JLabel("Bitte geben Sie den Speicherort der Druckdatei an:");
		lblBitteGebenSie.setVerticalAlignment(SwingConstants.TOP);
		lblBitteGebenSie.setBounds(12, 240, 478, 15);
		frame.getContentPane().add(lblBitteGebenSie);
		
		// Add actionlisteners:
		btnConvert.addActionListener(this);
		btnOpen.addActionListener(this);
		btnSave.addActionListener(this);
		
		mnFormat.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				switch(mode) {
				case "html":
					mntmSettingsHTML.setSelected(true);
					mntmSettingsRTF.setSelected(false);
					break;
				case "rtf":
					mntmSettingsHTML.setSelected(false);
					mntmSettingsRTF.setSelected(true);
					break;
				}
			}
		});
		
		mnItemStartConvert.setEnabled(false);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource() == this.btnOpen){ // OPEN
			fcOpen = new JFileChooser();
			xmlfilter = new FileNameExtensionFilter("Moodle XML-Dateien (*.xml)", "xml");
			fcOpen.setFileFilter(xmlfilter);
			fcOpen.setDialogTitle("Öffne Moodle XML-Datei ...");
			int returnVal = fcOpen.showOpenDialog(null);
			
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				String path = fcOpen.getSelectedFile().getAbsolutePath();
				txtFieldOpen.setText(path);
			}
		}

		if(ae.getSource() == this.btnSave){ // SAVE
			fcSave = new JFileChooser(txtFieldOpen.getText());
			fcSave.setDialogTitle("Speicherort für RTF-Dokument ...");
//			rtffilter = new FileNameExtensionFilter("RTF-Dokument (*.rtf)", "rtf");
//			fcSave.setFileFilter(rtffilter);
			int returnVal = fcSave.showSaveDialog(null);
			
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				String path = fcSave.getSelectedFile().getAbsolutePath();
				
				if(!path.endsWith("." + mode)) // append suffix
					path += "." + mode;
				
				txtFieldSave.setText(path);
			}
		}
		
		if(!txtFieldOpen.getText().isEmpty() && !txtFieldSave.getText().isEmpty()){
			btnConvert.setEnabled(true);
			mnItemStartConvert.setEnabled(true);
		}
		
		if(ae.getSource() == this.btnConvert || 
				(this.mnItemStartConvert.isEnabled() == true 
				 && ae.getSource() == this.mnItemStartConvert)){ // CONV
			String[] params = new String[3];
			params[0] = txtFieldOpen.getText();
			params[1] = txtFieldSave.getText();
			params[2] = mode;
			
			Main.main(params);
			
			lblOk.setVisible(true);
		}
	}
}
