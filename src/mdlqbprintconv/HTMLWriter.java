package mdlqbprintconv;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class HTMLWriter extends OutputWriter {
	
	private Writer writer;
	private Question currentQuestion;

	public HTMLWriter(String outFile) { // ctor
		openDocument(outFile);
	}
	
	public void openDocument(String outFile){
		try {
			writer = new OutputStreamWriter(new FileOutputStream(outFile), "ISO-8859-1");
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void printDocumentHeader(){
		try {
			writer.write("<!doctype html><html><head><meta charset=\"UTF-8\"/><title>Moodle Quiz Converter</title></head><body>\n");
		
			writer.write("<style>"
					+ "div {"
					//+ "    width: 300px;"
					//+ "    height: 100px;"
					+ "    background-color: #F0F0F0 ;"
					+ "    box-shadow: 10px 10px 5px #B8B8B8 ;"
					+ "    page-break-inside: avoid;"
					+ "}"
					+ "</style>");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void printDocumentFooter(){
		try {
			writer.write("</body></html>");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void printQuestion(Question q){
		// do not print twice:
		if (q == currentQuestion)
			return;
		currentQuestion = q;

		
		try {
			writer.write("<div>");
			writer.write("<h3> [" + escapeHTML(q.getTranslatedType()) + "]" + " " + escapeHTML(q.getName()) + "</h3>\n");
			writer.write("<h4>Frage: </h4>" + escapeHTML(q.getText().trim().replaceAll(" +", " ") + "\n"));
			writer.write("<h4>Feedback(s): </h4> " + escapeHTML(q.getFeedback().trim().replaceAll("\n", "").replaceAll(" +", " ") + "\n"));
			writer.write("</div>");
			writer.write("<br />");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
//		System.out.println("[" + q.getTranslatedType() + "]" + " " + q.getName());
//		System.out.println("Frage: " + q.getText());
//		System.out.println("Feedback(s): " + q.getFeedback());
		
//		System.out.println("\n---\n");
	}

	public void closeDocument() {
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String escapeHTML(String is){
		// http://www.rgagnon.com/javadetails/java-0306.html
		String s = is.replaceAll("\\s+", " "); // one or many backspaces
		StringBuffer sb = new StringBuffer();
		int n = s.length();
		for (int i = 0; i < n; i++) {
			char c = s.charAt(i);
			switch (c) {
			//case '<': sb.append("&lt;"); break;
			//case '>': sb.append("&gt;"); break;
	         case '&': sb.append("&amp;"); break;
	         case '"': sb.append("&quot;"); break;
	         case 'à': sb.append("&agrave;");break;
	         case 'À': sb.append("&Agrave;");break;
	         case 'â': sb.append("&acirc;");break;
	         case 'Â': sb.append("&Acirc;");break;
	         case 'ä': sb.append("&auml;");break;
	         case 'Ä': sb.append("&Auml;");break;
	         case 'å': sb.append("&aring;");break;
	         case 'Å': sb.append("&Aring;");break;
	         case 'æ': sb.append("&aelig;");break;
	         case 'Æ': sb.append("&AElig;");break;
	         case 'ç': sb.append("&ccedil;");break;
	         case 'Ç': sb.append("&Ccedil;");break;
	         case 'é': sb.append("&eacute;");break;
	         case 'É': sb.append("&Eacute;");break;
	         case 'è': sb.append("&egrave;");break;
	         case 'È': sb.append("&Egrave;");break;
	         case 'ê': sb.append("&ecirc;");break;
	         case 'Ê': sb.append("&Ecirc;");break;
	         case 'ë': sb.append("&euml;");break;
	         case 'Ë': sb.append("&Euml;");break;
	         case 'ï': sb.append("&iuml;");break;
	         case 'Ï': sb.append("&Iuml;");break;
	         case 'ô': sb.append("&ocirc;");break;
	         case 'Ô': sb.append("&Ocirc;");break;
	         case 'ö': sb.append("&ouml;");break;
	         case 'Ö': sb.append("&Ouml;");break;
	         case 'ø': sb.append("&oslash;");break;
	         case 'Ø': sb.append("&Oslash;");break;
	         case 'ß': sb.append("&szlig;");break;
	         case 'ù': sb.append("&ugrave;");break;
	         case 'Ù': sb.append("&Ugrave;");break;         
	         case 'û': sb.append("&ucirc;");break;         
	         case 'Û': sb.append("&Ucirc;");break;
	         case 'ü': sb.append("&uuml;");break;
	         case 'Ü': sb.append("&Uuml;");break;
	         case '®': sb.append("&reg;");break;         
	         case '©': sb.append("&copy;");break;   
	         case '': sb.append("&euro;"); break;
			// be carefull with this one (non-breaking whitee space)
			//case ' ': sb.append("&nbsp;");break;         
			default:  sb.append(c); break;
			}
		}
		return sb.toString().replaceAll("[^\\x20-\\x7e]", ""); // replace nonprintable ascii chars with nothing
	}
}


