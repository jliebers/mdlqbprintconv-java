package mdlqbprintconv;

import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public class QuizContentHandler implements ContentHandler {

	private OutputWriter writer;
	private Question question;
	private String currentValue;
	private Stack<String> currentElement;
	
	public QuizContentHandler (OutputWriter writer){
		if(writer == null){
			System.err.println("QuizContentHandler was initialized with null.");
			System.exit(-1);
		}
		
		this.writer = writer;
		currentElement = new Stack<String>();
	}
	

	@Override
	public void startDocument() throws SAXException {
		writer.printDocumentHeader();
	}

	@Override
	public void endDocument() throws SAXException {
		writer.printDocumentFooter();
		writer.closeDocument();
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		currentValue = new String(ch, start, length);
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
		currentElement.push(localName);
		
		if (localName.equals("question") && !atts.getValue(0).equals("category")){
			if(question != null && question.isComplete()){
				writer.printQuestion(question);
			}
			
			
			question = new Question();
			question.setType(atts.getValue(0));
		}
	}
	
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		currentElement.pop();
		
		if (localName.equals("text")){
			switch(currentElement.peek()) {
			case "name":
				//question.setName(currentValue.replaceAll("\\<[^>]*>",""));
				question.setName(currentValue);
				break;
			case "questiontext":
				question.setText(currentValue);
				break;
			case "feedback":
				question.setFeedback(question.getFeedback() + " " + currentValue);
				break;
			case "generalfeedback":
				question.setFeedback(question.getFeedback() + " " + currentValue);
				break;
			}
		}
		

		
	}

	@Override
	public void endPrefixMapping(String prefix) throws SAXException {}

	@Override
	public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {}
	
	@Override
	public void processingInstruction(String target, String data) throws SAXException {}

	@Override
	public void setDocumentLocator(Locator locator) {}

	@Override
	public void skippedEntity(String name) throws SAXException {}

	@Override
	public void startPrefixMapping(String prefix, String uri) throws SAXException{}

}
